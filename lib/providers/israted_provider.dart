import 'package:feex/constants.dart';
import 'package:feex/models/carousel_image_model.dart';
import 'package:feex/models/service_details_data_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

//provider class for topcategories provider

class IsRatedProvider extends ChangeNotifier {
  bool _israted = true;
  bool _error = false; // error handling
  String _errorMessage = ''; // error handling

  //getters for data
  bool get israted => _israted;

  bool get error => _error;

  String get errorMessage => _errorMessage;

  // Async Function for fetching...returns datatype of
  // List<TopCategoriesDataModel>

  fetchcarouselimages() async {
    //Runs http request

    var response = await http.get(
      Uri.parse(host + '/api/rateds'),
    ); //fetches all the top services

    if (response.statusCode == 200) {
      try {
        var jsonResponse = json.decode(response.body)['data'];
        //converts data into a list for easy breakdown

        //Maps data as a list of data model
        print(jsonResponse);
        _israted = jsonResponse[0]['attributes']['israted'];
        _error = false;
      } catch (e) {
        _error = true;
        _errorMessage = e.toString();
        _israted = true;
      }
    } else {
      _error = true;
      _errorMessage = 'Unknown';
      _israted = true;
    }
    notifyListeners();
  }

  void initialValues() {
    _israted = true;
    _error = false;
    _errorMessage = '';
    notifyListeners();
  }
}
