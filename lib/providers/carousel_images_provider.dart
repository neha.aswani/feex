import 'package:feex/constants.dart';
import 'package:feex/models/carousel_image_model.dart';
import 'package:feex/models/service_details_data_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

//provider class for topcategories provider

class CarouselImagesProvider extends ChangeNotifier {
  List<CarouselImageModel> _carouselimages = []; // for data as list
  bool _error = false; // error handling
  String _errorMessage = ''; // error handling

  //getters for data
  List<CarouselImageModel> get carouselimages => _carouselimages;

  bool get error => _error;

  String get errorMessage => _errorMessage;

  // Async Function for fetching...returns datatype of
  // List<TopCategoriesDataModel>

  fetchcarouselimages() async {
    //Runs http request

    var response = await http.get(
      Uri.parse(host + '/api/carouselimages?populate=*'),
    ); //fetches all the top services

    if (response.statusCode == 200) {
      try {
        var jsonResponse = json.decode(response.body)['data'] as List;
        //converts data into a list for easy breakdown

        //Maps data as a list of data model

        _carouselimages =
            jsonResponse.map((e) => CarouselImageModel.fromJson(e)).toList();
        _error = false;
      } catch (e) {
        _error = true;
        _errorMessage = e.toString();
        _carouselimages = [];
      }
    } else {
      _error = true;
      _errorMessage = 'Unknown';
      _carouselimages = [];
    }
    notifyListeners();
  }

  void initialValues() {
    _carouselimages = [];
    _error = false;
    _errorMessage = '';
    notifyListeners();
  }
}
