class CarouselImageModel {
  final String url;

  CarouselImageModel({this.url = ''});

  factory CarouselImageModel.fromJson(Map<String, dynamic> json) {
    return CarouselImageModel(
        url: json['attributes']['image']['data'][0]['attributes']['url']);
  }
}
