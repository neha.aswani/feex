class TopCategoriesDataModel {
  final int id;
  final String Category;

  TopCategoriesDataModel({this.id, this.Category});
  factory TopCategoriesDataModel.fromJson(Map<String, dynamic> json) {
    return TopCategoriesDataModel(id: json['id'], Category: json['name']);
  }
}
