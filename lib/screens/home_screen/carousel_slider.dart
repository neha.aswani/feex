import 'package:feex/constants.dart';
import 'package:feex/providers/carousel_images_provider.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class CarouselImageSlider extends StatelessWidget {
  const CarouselImageSlider({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    context.read<CarouselImagesProvider>().fetchcarouselimages();
    return Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height * 0.25,
        child:
            Consumer<CarouselImagesProvider>(builder: (context, value, child) {
          return value.carouselimages.isEmpty && !value.error
              ? Shimmer.fromColors(
                  baseColor: Colors.grey.shade300,
                  highlightColor: Colors.white,
                  enabled: true,
                  child: CarouselSlider.builder(
                    itemCount: 5,
                    itemBuilder: (BuildContext context, int itemIndex, _) =>
                        Container(
                      margin: const EdgeInsets.all(0),
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      width: MediaQuery.of(context).size.width,
                      height: 180,
                    ),

                    //Slider Container properties
                    options: CarouselOptions(
                      height: MediaQuery.of(context).size.height * 0.21,
                      enlargeCenterPage: true,
                      autoPlay: true,
                      aspectRatio: 16 / 9,
                      autoPlayCurve: Curves.fastOutSlowIn,
                      enableInfiniteScroll: true,
                      autoPlayAnimationDuration:
                          const Duration(milliseconds: 800),
                      viewportFraction: 0.8,
                    ),
                  ),
                )
              : CarouselSlider.builder(
                  itemCount: value.carouselimages.length,
                  itemBuilder: (BuildContext context, int itemIndex, _) =>
                      Container(
                    margin: const EdgeInsets.only(left: 30, right: 30),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: value.carouselimages.isNotEmpty
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(15),
                            child: FadeInImage.memoryNetwork(
                              placeholder: kTransparentImage,
                              image: host + value.carouselimages[itemIndex].url,
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height * 0.21,
                              fit: BoxFit.fill,
                            ),
                          )
                        : const SizedBox(
                            child: const CircularProgressIndicator(),
                            height: 5,
                            width: 50,
                          ),
                  ),

                  //Slider Container properties
                  options: CarouselOptions(
                    height: MediaQuery.of(context).size.height * 0.21,
                    enlargeCenterPage: false,
                    autoPlay: true,
                    aspectRatio: 16 / 9,
                    autoPlayCurve: Curves.fastOutSlowIn,
                    enableInfiniteScroll: true,
                    autoPlayAnimationDuration:
                        const Duration(milliseconds: 800),
                    viewportFraction: 1,
                  ),
                );
        }));
  }
}
